let desc = document.querySelector("#desc")
let images = ["img/angular.svg", "img/aurelia.svg", "img/backbone.svg",
"img/ember.svg", "img/react.svg", "img/vue.svg"]


function double_image(){
    let double_image = images.concat(images)
    for(let i=double_image.length-1; i>0; i--){
        let temp = double_image[i]

        let r = Math.floor(Math.random()*(i-1))

        double_image[i] = double_image[r]
        double_image[r] = temp
    }
    return double_image
}

function create_cards(){

    let d_images = double_image()

    for(let i = 0; i < d_images.length; i++){
        let card = document.createElement("div")

        let front = document.createElement("img")
        front.setAttribute("src", "img/js-badge.svg")

        let back = document.createElement("img")
        back.setAttribute("src", `${d_images[i]}`)

        front.classList.add("front")
        back.classList.add("back")

        card.classList.add("card")

        card.appendChild(front)
        card.appendChild(back)

        desc.appendChild(card)

        card.addEventListener('click', clip_card)
    }
}

create_cards()

let clicked_img = []
let lock = false

function clip_card(){
    if(lock) return
    change_card(this, 1)
    clicked_img.push(this) 
    console.log(clicked_img)
    if(clicked_img.length == 2){
        lock = true;
        setTimeout(function(){
            if(clicked_img[0].lastChild.getAttribute('src') != clicked_img[1].lastChild.getAttribute('src')){
                    change_card(clicked_img[0])
                    change_card(clicked_img[1])
            }
            lock = false
            clicked_img = []
        }, 1500); 
    }   
}


function change_card(card, type){
    card.classList.toggle("flip")
    let front = card.firstChild
    let back = card.lastChild

    if(type){
        front.style.visibility = "hidden"
        back.style.visibility = "visible"
    }else{
        front.style.visibility = "visible"
        back.style.visibility = "hidden"
    }
}